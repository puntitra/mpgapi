# TODO: Switch to python:3.8-slim-buster and test
FROM python:3.8

# Create a new user and change work directory
ENV USERNAME=mpgapi
RUN useradd \
    --create-home \ 
    --shell /bin/bash \
    --no-log-init \
    "$USERNAME"
USER "$USERNAME"
WORKDIR /home/"$USERNAME"

# Install libraries
# Test related libraries are omitted
RUN pip install --upgrade pip
RUN pip install --no-cache-dir \
    python-dotenv==0.15.0 \
    flask==1.1.2 \
    pandas==1.1.5 \
    scikit-learn==0.23.2 \
    gunicorn==20.0.4 \ 
    apispec==4.0.0 \
    apispec-webframeworks==0.5.2 \
    marshmallow==3.10.0 \
    flask-swagger-ui==3.36.0

# Add necessary files for running the API
# This .env file contains only non-sensitive information
# Alternative: Include .dockerignore instead
COPY .env .env
COPY models/. models/.
RUN mkdir src
COPY src/config.py src/config.py
COPY src/wsgi.py src/wsgi.py
COPY src/core/. src/core/.
COPY src/utils/. src/utils/.
COPY src/blueprints/. src/blueprints/.

# Expose port and run
EXPOSE 5000

# Problem: Each command runs a separate sub-shell, therefore the environment variables are not
# preserved and user startup scripts are not sourced (in this case, .profile)
# Solutions: (1) pass in environment variable explicitly to set its value in the container
#            (2) source .profile in the same process where the desired command is executed
# Source: https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
# TODO (enhancement): Modify exec form (2) to json array form (1) for readability
CMD /bin/bash -c "source $HOME/.profile && gunicorn --bind 0.0.0.0:5000 wsgi:app --chdir src"
