"""
Filename: test_mpg.py
Description: prediction test endpoint.
             Note: This test suite does not test the accuracy of the predictions.
"""

import os
import json
import requests
import numpy as np
import fixture

"""
All Test Cases
--------------
1. valid single instance with ordered features
2. valid single instance with un-ordered features
3. single instance with non-numerical values
4. single instance with null value
5. single instance with missing feature
6. single instance with missing features
7. single instance with unmatched-case features
8. valid multiple instances with ordered features
9. valid multiple instances with un-ordered features
10. multiple instances with non-numerical values
11. multiple instances with null value
12. multiple instances with null values
13. multiple instances with null value in every instance
14. multiple instances with missing features in one instance
15. multiple instances with missing features in every instance
16. multiple instances with unmatched-case features in one instance
17. multiple instances with unmatched-case features in every instance
18. multiple instances with valid, un-ordered features, null values, missing features, unmatched-case features
"""

def test_predict_single(mpg_predict_url, testutils):
    """
    Test case 1: Request contains one valid instance with ordered features
    Expected behavior: status 200 and [float]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.valid1))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None not in preds


def test_predict_single_w_suffled_features(mpg_predict_url, testutils):
    """
    Test case 2: Request contains one valid instance with shuffled-order features
    Expected behavior: status 200 and [float]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.shuffle))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None not in preds


def test_predict_single_w_string_values(mpg_predict_url, testutils):
    """
    Test case 3: Request contains one instance with non-numerical values
    Expected behavior: status 200 and [None]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.some_string))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None == preds[0]


def test_predict_single_w_one_null(mpg_predict_url, testutils):
    """
    Test case 4: Request contains one instance containing a null value
    Expected behavior: status 200 and [None]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.na))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None == preds[0]


def test_predict_single_miss_one_feature(mpg_predict_url, testutils):
    """
    Test case 5: Request contains one instance with missing feature
    Expected behavior: status 200 and [None]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.miss_horsepower))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None == preds[0]


def test_predict_single_miss_features(mpg_predict_url, testutils):
    """
    Test case 6: Request contains one instance with missing features
    Expected behavior: status 200 and [None]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.miss_features))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None == preds[0]


def test_predict_single_unmatch_case_features(mpg_predict_url, testutils):
    """
    Test case 7: Request contains one instance with non-lowercase features
    Expected behavior: status 200 and [float]
    """
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(fixture.mixed_case_features1))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == 1 and None not in preds


def test_predict_multi(mpg_predict_url, testutils):
    """
    Test case 8: Request contains multiple valid instances.
    Expected behavior: status 200 and [float, float, float]
    """
    data = testutils.combine(fixture.valid3,
                             fixture.valid1,
                             fixture.valid2)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200

    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds)
    assert all_none and all_float


def test_predict_multi_w_shuffled_features(mpg_predict_url, testutils):
    """
    Test case 9: Request contains multiple instances (shuffled features).
    Expected behavior: status 200 and [float, float, float, float]
    """
    data = testutils.combine(fixture.valid3,
                             fixture.valid1,
                             fixture.shuffle,
                             fixture.valid2)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds)
    assert all_none and all_float


def test_predict_multi_w_string_values(mpg_predict_url, testutils):
    """
    Test case 10: Request contains multiple instances. One instance has string values
    Expected behavior: status 200 and [float, float, None, float]
    """
    data = testutils.combine(fixture.valid1,
                             fixture.valid2,
                             fixture.some_string,
                             fixture.valid3)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[2])
    assert all_none and all_float


def test_predict_multi_w_one_null(mpg_predict_url, testutils):
    """
    Test case 11: Request contains multiple instances. One instance has a null value
    Expected behavior: status 200 and [float, float, None, float]
    """
    data = testutils.combine(fixture.valid1,
                             fixture.valid2,
                             fixture.na,
                             fixture.valid3)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[2])
    assert all_none and all_float


def test_predict_multi_w_nulls(mpg_predict_url, testutils):
    """
    Test case 12: Request contains multiple instances. Two instances have null value(s).
    Expected behavior: status 200 and [float, float, None, float]
    """
    data = testutils.combine(fixture.na,
                             fixture.valid1,
                             fixture.valid2,
                             fixture.na_multi,
                             fixture.valid3)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[0, 3])
    assert all_none and all_float


def test_predict_multi_w_null_every_instance(mpg_predict_url, testutils):
    """
    Test case 13: Request contains multiple instances. Every instance has null value(s).
    Expected behavior: status 200 and [None, None, None]
    """
    data = testutils.combine(fixture.na_multi,
                             fixture.na_blank,
                             fixture.na)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[0, 1, 2])
    assert all_none and all_float


def test_predict_multi_w_miss_features_one_instance(mpg_predict_url, testutils):
    """
    Test case 14: Request contains multiple instances. One instance misses features.
    Expected behavior: status 200 and [float, float, None, float]
    """
    data = testutils.combine(fixture.valid1,
                             fixture.valid3,
                             fixture.miss_features,
                             fixture.valid2)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[2])
    assert all_none and all_float


def test_predict_multi_w_miss_features_every_instance(mpg_predict_url, testutils):
    """
    Test case 15: Request contains multiple instances. Every instance misses feature(s).
    Expected behavior: status 200 and [None, None]
    """
    data = testutils.combine(fixture.miss_features,
                             fixture.miss_horsepower)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[0, 1])
    assert all_none and all_float


def test_predict_multi_w_unmatched_case_features_one_instance(mpg_predict_url, testutils):
    """
    Test case 16: Request contains multiple instances. One instance has unmatched-case features.
    Expected behavior: status 200 and [float, float, float]
    """
    data = testutils.combine(fixture.valid1,
                             fixture.mixed_case_features1,
                             fixture.valid3)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds)
    assert all_none and all_float


def test_predict_multi_w_unmatched_case_features_every_instance(mpg_predict_url, testutils):
    """
    Test case 17: Request contains multiple instances. All instances have unmatched-case features.
    Expected behavior: status 200 and [float, float]
    """
    data = testutils.combine(fixture.mixed_case_features2,
                             fixture.mixed_case_features1)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds)
    assert all_none and all_float


def test_predict_multi_w_unmatched_case_features_every_instance(mpg_predict_url, testutils):
    """
    Test case 18: Request contains multiple instances. Some instances contain valid,
                  un-ordered features, null values, non-numeric features, missing features,
                  and unmatched-case features.
    Expected behavior: status 200 and
                       [float, None, None, float, float, None, float, None, float]
    """
    data = testutils.combine(fixture.mixed_case_features2,
                             fixture.na_multi,
                             fixture.some_string,
                             fixture.shuffle,
                             fixture.valid3,
                             fixture.miss_features,
                             fixture.mixed_case_features1,
                             fixture.na_blank,
                             fixture.valid2)
    resp = requests.post(url=mpg_predict_url,
                         headers=testutils.get_headers(),
                         data=json.dumps(data))

    assert resp.status_code == 200
    preds = resp.json()
    assert len(preds) == len(data)
    all_none, all_float = testutils.is_valid_prediction(preds, none_indices=[1, 2, 5, 7])
    assert all_none and all_float
