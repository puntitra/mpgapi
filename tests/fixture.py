"""
Filename: fixture.py
Description: test data
"""

valid1 = [{"cylinders": "8",
                "horsepower": "307.0",
                "displacement": "1.8",
                "weight": "2500",
                "acceleration": "22.0",
                "year": "70",
                "origin": "1",
                "name": "chevrolet"}]

valid2 = [{"cylinders": "4",
                "horsepower": "215.0",
                "displacement": "1.8",
                "weight": "1500",
                "acceleration": "12.0",
                "year": "85",
                "origin": "3",
                "name": "nissan"}]

valid3 = [{"cylinders": "8",
                "horsepower": "215.0",
                "displacement": "1.8",
                "weight": "3000",
                "acceleration": "12.0",
                "year": "71",
                "origin": "3",
                "name": "nissan"}]

shuffle = [{"weight": "3000",
                "acceleration": "12.0",
                "cylinders": "8",
                "year": "71",
                "origin": "3",
                "horsepower": "215.0",
                "displacement": "1.8",
                "name": "nissan"}]

na = [{"cylinders": "8",
             "horsepower": "307.0",
             "displacement": "1.8",
             "weight": "1500",
             "acceleration": "12.0",
             "year": "?",
             "origin": "1",
             "name": "chevrolet"}]

na_multi = [{"cylinders": "?",
             "horsepower": "307.0",
             "displacement": "1.8",
             "weight": "?",
             "acceleration": "12.0",
             "year": "?",
             "origin": "1",
             "name": "chevrolet"}]

na_blank = [{"cylinders": "8",
             "horsepower": "307.0",
             "displacement": "1.8",
             "weight": "1500",
             "acceleration": "12.0",
             "year": "",
             "origin": "1",
             "name": "chevrolet"}]

all_numeric = [{"cylinders": 8,
                     "horsepower": 307.0,
                     "displacement": 1.8,
                     "weight": 1500,
                     "acceleration": 12.0,
                     "year": 70,
                     "origin": 1,
                     "name": "chevrolet"}]

some_string = [{"cylinders": 8,
                     "horsepower": "xx",
                     "displacement": "1.8",
                     "weight": "1500",
                     "acceleration": 12.0,
                     "year": "xx",
                     "origin": "1",
                     "name": "chevrolet"}]

miss_horsepower = [{"cylinders": "8",
                    "displacement": "1.8",
                    "weight": "3000",
                    "acceleration": "12.0",
                    "year": "71",
                    "origin": "3",
                    "name": "nissan"}]

miss_features = [{"cylinders": "8",
                  "displacement": "1.8",
                  "weight": "3000",
                  "acceleration": "12.0",
                  "year": "71",
                  "name": "nissan"}]

mixed_case_features1 = [{"CylInders": "4",
                "horsepOwer": "215.0",
                "displAcement": "1.8",
                "Weight": "1500",
                "acceLeration": "12.0",
                "yeaR": "85",
                "oriGin": "3",
                "NAME": "nissan"}]

mixed_case_features2 = [{"cylinders": "8",
                "HORSepower": "307.0",
                "displaceMENt": "1.8",
                "weIght": "2500",
                "ACceleratION": "22.0",
                "yEar": "70",
                "ORigin": "1",
                "name": "chevrolet"}]
