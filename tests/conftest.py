"""
Filename: conftest.py
Description: Test fixtures for all tests
"""

import os
import pytest
import requests


SCOPE = 'session'
with open(os.path.join('/tmp/apikey', 'apikey.txt')) as f:
    APIKEY = f.readline().strip()

############################# Pytest custom option ############################

def pytest_addoption(parser):
    # pass option to test API on different hosts
    parser.addoption('--host', action='store', default='http://localhost:5000')


@pytest.fixture(scope=SCOPE)
def host(request):
    return request.config.getoption('--host')


###############################################################################


# Test helpers
class TestUtils:

    @staticmethod
    def get_headers(content_type='application/json', cache_control='no-cache', api_key=APIKEY):

        headers = {'content-type': content_type,
                   'cache-control': cache_control,
                   'apikey': api_key,
                  }
        return headers

    @staticmethod
    def combine(*lists):

        num_list = len(lists)

        if num_list == 0:
            return []
        elif num_list == 1:
            return lists[0]
        else:
            data = []
            for each in lists:
                data.extend(each)
            return data

    @staticmethod
    def is_valid_prediction(preds, none_indices=None):
        """
        Validates prediction

        Parameters:
        preds -- list of predictions
        none-indices -- list of indices that are expected to be None

        Returns:
        check_none -- True if prediction contains all expected None, otherwise False
        check_float -- True if prediction contains all expected float, otherwise False
        """
        if none_indices == None:
            none_indices = []

        all_must_be_none = [ preds[i] for i in none_indices ]
        all_must_be_float = [i for j, i in enumerate(preds) if j not in none_indices ]

        check_none = all_must_be_none.count(None) == len(all_must_be_none) # fastest
        check_float = all(isinstance(x, float) for x in all_must_be_float)

        return check_none, check_float


@pytest.fixture
def testutils():
    return TestUtils


@pytest.fixture(scope=SCOPE)
def tbd_test_url(host):
    return os.path.join(host, 'tbd', 'test')


@pytest.fixture(scope=SCOPE)
def mpg_test_url(host):
    return os.path.join(host, 'mpg', 'test')


@pytest.fixture(scope=SCOPE)
def mpg_predict_url(host):
    return os.path.join(host, 'mpg', 'predict')


@pytest.fixture(scope=SCOPE)
def api_v1_host(host):
    return os.path.join(host, 'api', 'v1')
