"""
Filename: test_endpoints.py
Description: general health check for all endpoints
"""

import os
import requests
from openapi_spec_validator import validate_spec_url


############################# blueprint_mpg #################################


def test_blueprint_mpg_test(mpg_test_url, testutils):

    response = requests.get(mpg_test_url)

    assert response.status_code == 200

    json = response.json()
    assert 'message' in json
    assert json['message'] == 'test endpoint of blueprint_mpg'


def test_blueprint_mpg_predict_post_no_apikey(mpg_predict_url, testutils):

    response = requests.post(url=mpg_predict_url)

    # sending a POST request without valid apikey in the header to 
    # predict endpoint should get 401 response (Unauthorized)
    assert response.status_code == 401


def test_blueprint_mpg_predict_post_no_data(mpg_predict_url, testutils):

    response = requests.post(url=mpg_predict_url, headers=testutils.get_headers())

    # sending a POST request without valid data to predict endpoint 
    # should get 400 response (Bad Request)
    assert response.status_code == 400


def test_blueprint_mpg_predict_get(mpg_predict_url, testutils):

    response = requests.get(mpg_predict_url)

    # sending a GET request without valid data to predict endpoint should get 500 response
    assert response.status_code == 405


############################# blueprint_tbd #################################


def test_blueprint_tbd_test(tbd_test_url, testutils):

    response = requests.get(tbd_test_url)

    assert response.status_code == 200

    json = response.json()
    assert 'message' in json
    assert json['message'] == 'test endpoint of blueprint_tbd'


############################## swagger spec #################################


# def test_swagger_specification(host):
#     endpoint = os.path.join(host, 'api', 'swagger.json')
#     validate_spec_url(endpoint)


