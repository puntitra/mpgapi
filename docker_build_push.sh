#!/bin/bash
# This script builds, tags, and pushes Docker image for deploying mpgapi project

ADDRESS=gcr.io
PROJECT_ID=plucky-hue-295819
REPOSITORY=auto
VERSION=0.1.2

# Build
docker build -t ${PROJECT_ID}:${VERSION} .
ID="$(docker images | grep ${PROJECT_ID} | head -1 | awk '{print $3}')"

# Tag
docker tag ${ID} ${ADDRESS}/${PROJECT_ID}/${REPOSITORY}:${VERSION}

# Push
docker push ${ADDRESS}/${PROJECT_ID}/${REPOSITORY}:${VERSION}
