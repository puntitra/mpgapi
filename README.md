[TOC]

# MPG API: End-to-end Flask API

This repository offers an end-to-end Flask application for HTTP API for predicting miles per gallon using a Gradient Boosting Regressor model. It includes source code, test scripts, and deployment scripts.

Checkout [mpgexp](https://puntitra@bitbucket.org/puntitra/mpgexp.git) repository for modeling portion of this problem.

## Screenshot

API Specification is shown below: 

![MPG API Specification](docs/swaggerui.PNG)

## Technology

MPG API is implemented with Flask. Pytest and Postman were used for testing. It is deployed with gunicorn, Docker, and Kubernetes on Google Cloud.

![Overall Stack](docs/arch.png)

## Features

* Structured with blueprints for scalability 
* Custom logging and error handling
* Development and production configurations
* API key storage using Kubernetes Secrets through mounted volume
* Automated tests for endpoints
* API documentation with OpenAPI v3 and Swagger UI
* Docker and Kubernetes scripts for deploying to Google Cloud

## Setup

Clone this repository.

```bash
git clone https://puntitra@bitbucket.org/puntitra/mpgapi.git

```

Change working mpgapi directory.
```bash
cd mpgapi
```

Install packages. *Note:* Python3 is required. Also, using virtual environment is highly recommended.
```bash
python3 -m pip install requirements.txt
```

.env contains Flask environment variables. For running in development mode, assign *development* to `FLASK_ENV`. For production mode, *production* should be assigned. To run in debug mode, set `FLASK_DEBUG` to *1*. Log level can be changed in src/config.py.

Run application with Flask's built-in server. (NOT recommended for production)

```bash
flask run
```
Run application with gunicorn.

```bash
gunicorn --bind localhost:5000 wsgi:app --chdir src
```

## Tests

Pytest test fixtures and scripts are under tests/.

Screenshot of pytest:

![Pytest](docs/pytest.png)

Screenshot of Postman:

![Postman](docs/postman.PNG)

*Note:* `curl` is a good alternative for quick test. A simple Python script with `requests` and `json` also works fine.
