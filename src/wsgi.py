"""
Filename: wsgi.py
Description: Application's entry point
"""

from os import environ, path
from dotenv import load_dotenv
from config import DevConfig, ProdConfig
from flask import jsonify
from src.core.app import create_app
from src.core.api_spec import spec


load_dotenv(path.join('..', '.env'))


if environ.get('FLASK_ENV') == 'development':
    app = create_app(config=DevConfig)
else:
    app = create_app(config=ProdConfig)

########################### API Spec ###########################
with app.test_request_context():
    for func in app.view_functions:
        if func == 'static':
            continue
        app.logger.info('Loading swagger docs for {}'.format(func))
        view_func = app.view_functions[func]
        spec.path(view=view_func)


@app.route('/api/swagger.json')
def create_swagger_spec():
    return jsonify(spec.to_dict())
###############################################################


if __name__ == "__main__":
    app.run()
