"""
Filename: load.py
Description: Pre-load model, ordered features, ColumnTransformer object.
"""
from flask import current_app
import pickle
import pandas as pd


def get_model(model_path=current_app.config['MODEL_PATH']):
    """
    Loads prediction model
    """
    current_app.logger.debug('Loading {}'.format(model_path))
    model = pickle.load(open(model_path, 'rb'))
    current_app.logger.debug('{} has been successfully loaded.'.format(model_path))
    return model


def get_preprocessor(pp_path=current_app.config['PREPROCESSOR_PATH']):
    """
    Loads ColumnTransformer
    """
    current_app.logger.debug('Loading {}'.format(pp_path))
    preprocessor = pickle.load(open(pp_path, 'rb'))
    current_app.logger.debug('{} has been successfully loaded.'.format(pp_path))
    return preprocessor


def get_features(tn_sample_path=current_app.config['SAMPLE_TRAIN_PATH']):
    """
    Loads ordered training columns
    """
    current_app.logger.debug('Reading {}'.format(tn_sample_path))
    X_tn_sample_df = pd.read_csv(tn_sample_path, header=0, index_col=0)
    features = X_tn_sample_df.columns
    current_app.logger.debug('Ordered features include {}'.format(', '.join(features)))
    return features


if __name__ == '__main__':
    model = get_model()
    pp = get_preprocessor()
    features = get_features()
