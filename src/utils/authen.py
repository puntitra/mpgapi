"""
Filename: authen.py
Description: Utility functions for authenticating requests
"""
from functools import wraps
from flask import current_app, request, abort
from werkzeug.exceptions import Unauthorized


def apikey_required(view_func):
    """
    Decorator function for validating API key.
    It compares apikey passed in the request headers to the value read from the mounted volume.
    """
    @wraps(view_func)
    def decorated_func(*args, **kwargs):

        if request.headers.get('apikey') == current_app.config['APIKEY']:
            current_app.logger.debug('API key validated: valid')
            return view_func(*args, **kwargs)
        else:
            current_app.logger.debug('API key validated: invalid or none')
            # TODO (minor): Use custom error handler
            abort(401, description="Valid API key is required")

    return decorated_func
