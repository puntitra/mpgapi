"""
Filename: prepare.py
Description: helper functions for cleaning input data
"""
from flask import current_app
import pandas as pd
import numpy as np
from src.utils.load import get_features, get_preprocessor


features = get_features()
preprocessor = get_preprocessor()


def preprocess(json):
    """
    Cleans and preprocesses input request (json)

    Parameter:
    json -- request data in json format

    Returns:
    X -- Preprocessed array of length p, p <= n
    pred_list -- Boolean list of size n, where n is number of instances of the request.
                 True means there will be a prediction.
                 False means the row has been dropped during preprocessing.
    """
    # Convert json to DataFrame
    df = json_to_df(json)
    request_len = df.shape[0]

    # Clean df
    current_app.logger.debug('Removing rows with missing value')
    df, dropped_rows = clean_df(df, alpha_cols=['name'])

    # TODO (enhancement): Translate origin to 1, 2, 3

    # Apply ColumnTransformer
    if df.shape[0] >= 1:
        current_app.logger.debug('Applying ColumnTransformer')
        X = preprocessor.transform(df)
    else:
        current_app.logger.warning('Every instance has been dropped due to missing value(s). Cannot make any prediction.')
        # raise ValueError('Every instance has been dropped due to missing value(s). Cannot preprocess and predict.')
        return None, [None] * request_len

    # Generate a list of length equals to the original json.
    # This list will be used to fill predictions later.
    pred_list = [True] * request_len
    for i in dropped_rows:
        pred_list[i] = False

    return X, pred_list


def json_to_df(json):
    """
    Converts json to DataFrame with column names. Support case-insensitive feature names

    Parameter:
    json -- request data in json format

    Returns:
    df -- pandas DataFrame with ordered columns converted from json data
    """
    current_app.logger.debug('Converting json to DataFrame')
    # Make sure all features are lowercases
    json = eval(repr(json).lower())
    df = pd.DataFrame(json, columns=features)
    current_app.logger.debug('Input shape: {}'.format(str(df.shape)))

    if df.shape[0] == 0:
        # Note: another viable behavior is to just return an empty list and add warning log.
        raise ValueError('Request does not contain any instance. Each request must contain one instance at a minimum.')

    return df


def clean_df(df, alpha_cols=None):
    """
    Removes row(s) with invalid values. Assumption: invalid values are non-numerical values
    for all features not passed in as alpha_cols.
    Removes row(s) with missing values. Assumption: missing values can either be ? or NaN.

    Parameters:
    df -- pandas DataFrame of request data
    alpha_cols -- (optional) list of features to be excluded from numerical check.

    Returns:
    df -- pandas DataFrame with no missing value.
    rows_with_nan -- row indices that have been dropped.

    >>> df
          a    b    c
    0     ?  3.2    6
    1     9    y  8.6
    2   0.5    1  cow
    3   0.3  1.1    4
    4   0.3  NaN    4

    df = clean_df(df, alpha_cols=['b'])

    will return df below and [0, 2, 4]

    >>> df
          a    b    c
    0     9    y  8.6
    1   0.3  1.1    4

    Explanation: row 0 of input df contains ?, which is replaced by nan
                 row 1 remains in df because column b is excluded from numerical checking
                 row 4 of input df contains nan originally

                 [0,2, 4] is list of row indices that were dropped. It is needed for 
                          returning full predictions.
    """
    current_app.logger.debug('(before) Shape: {}'.format(str(df.shape)))

    nan_rows = df[df.isnull().any(axis=1)].shape[0]
    current_app.logger.debug('{} row(s) will be dropped due to missing feature(s).'.format(nan_rows))

    # Numerical hecking - change non-numeric values to nan
    if alpha_cols is not None:

        assert isinstance(alpha_cols, list)

        numeric_cols = [ each for each in features if each not in alpha_cols ]
        if len(numeric_cols) < len(features):
            current_app.logger.debug('Checking row(s) with invalid values. Exclusion: {}'.format(', '.join(alpha_cols)))
            df[numeric_cols] = df[numeric_cols][df[numeric_cols].applymap(is_number)]
            diff_nan_rows = df[df.isnull().any(axis=1)].shape[0] - nan_rows
            current_app.logger.debug('{} row(s) will be dropped due to invalid value(s).'.format(diff_nan_rows))

    # Missing value checking - change ? to nan
    df.replace('?', np.nan, inplace=True)
    rows_with_nan = list(df.loc[pd.isnull(df).any(1), :].index.values)
    if len(rows_with_nan) > 0:
        current_app.logger.debug('Dropping {} row(s).'.format(len(rows_with_nan)))
        df.dropna(inplace=True)

    current_app.logger.debug('(after) Shape: {}'.format(str(df.shape)))

    return df, rows_with_nan


def is_number(x):
    try:
        float(x)
        return True
    except:
        return False


def translate_origin(origin):

    origin = str(origin).lower()

    if origin.isnumeric():
        return origin

    if origin == 'north america':
        return '1'
    elif origin == 'europe':
        return '2'
    elif origin == 'asia':
        return '3'
    else:
        return '0'
