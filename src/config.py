"""
Filename: config.py
Description: config file
"""
import os
import sys


class BaseConfig:
    """ Base configuration """
    APP_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    sys.path.append(APP_DIR)
    LOG_FILENAME = 'mpgapi.log'
    # Read key from file under mounted volume
    with open(os.path.join('/tmp/apikey', 'apikey.txt')) as f:
        APIKEY = f.readline().strip()
    ############### Config for preprocess & predict ################
    MODEL_DIR = os.path.join(APP_DIR, 'models')
    MODEL_PATH = os.path.join(MODEL_DIR, 'gbr_model.pkl')
    PREPROCESSOR_PATH = os.path.join(MODEL_DIR, 'preprocessor.pkl')
    SAMPLE_TRAIN_PATH = os.path.join(MODEL_DIR, 'X_tn_sample.csv')
    ################################################################


# Note: DEBUG is overriden by FLASK_DEBUG and FLASK_ENV
# https://flask.palletsprojects.com/en/1.0.x/config/#DEBUG
class DevConfig(BaseConfig):
    LOG_LEVEL = 'debug'
    DEBUG = True
    TESTING = True


class ProdConfig(BaseConfig):
    LOG_LEVEL = 'error'
    DEBUG = False
    TESTING = False
