from flask import Blueprint, current_app, jsonify, request
import pickle
import pandas as pd
import numpy as np
from src.utils.authen import apikey_required
from src.utils.load import get_model
from src.utils.prepare import preprocess


# Blueprint Definition
blueprint_mpg = Blueprint('blueprint_mpg', import_name=__name__)


# Pre-load model object
model = get_model()


@blueprint_mpg.route('/test', methods=['GET'])
def test():
    """
    ---
    get:
      description: mpg test endpoint
      responses:
        '200':
          description: request successful
          content:
            application/json:
              schema: TestOutputSchema
      tags:
      - test
    """
    current_app.logger.debug('test endpoint of blueprint_mpg')
    output = {'message': 'test endpoint of blueprint_mpg'}
    return jsonify(output)


@blueprint_mpg.route('/predict', methods=['POST'])
@apikey_required
def predict():
    # TODO: convert yaml docstring to marshmallow schema
    """
    ---
    post:
      tags:
      - predict
      description: predicts miles per gallon
      requestBody:
        content:
          application/json:
            schema:
              type: array
              items:
                properties:
                  cylinders:
                    type: integer
                    required: true
                    example: 4
                  displacement:
                    type: number
                    required: true
                    example: 305.0
                  horsepower:
                    type: number
                    required: true
                    example: 130.0
                  weight:
                    type: number
                    required: true
                    example: 1500.0
                  acceleration:
                    type: number
                    required: true
                    example: 12.0
                  year:
                    type: integer
                    required: true
                    example: 98
                  origin:
                    type: integer
                    required: true
                    example: 1
                    minimum: 0
                    maximum: 3
                  name:
                    type: string
                    required: true
                    example: nissan sentra
      responses:
        '200':
          description: request successful
          content:
            application/json:
              schema:
                type: array
                items:
                  type: PredictOutputSchema
                  example: 33.87613
    """
    current_app.logger.debug('predict endpoint of blueprint_mpg')

    # Preprocess
    X, full_pred  = preprocess(request.get_json())
    current_app.logger.debug('(before): ' + str(full_pred))

    # if every instance has been dropped, go ahead all return full_pred (all nan)
    if X is None:
        current_app.logger.debug('Responding with None only.')
        return jsonify(full_pred)

    # Predict
    current_app.logger.debug('Predicting gas mileage')
    try:
        preds = list(model.predict(X))
        current_app.logger.debug(preds)
    except ValueError as e:
        current_app.errorhandler(e)

    # Replace each True element in full_pred with actual prediction
    for i, each in enumerate(full_pred):
        full_pred[i] = preds.pop(0) if each else None

    current_app.logger.debug('(after): ' + str(full_pred))

    # Return prediction
    return jsonify(full_pred)
