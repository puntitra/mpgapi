"""
Filename: blueprint_swaggerui.py
Description: swagger UI blueprint definition
"""

from flask_swagger_ui import get_swaggerui_blueprint

SWAGGER_URL = '/api/docs'
API_URL = '/api/swagger.json'

# Create blueprint
blueprint_swaggerui = get_swaggerui_blueprint(SWAGGER_URL,
                                              API_URL,
                                              config={'app_name': 'MPG API'}
                                             )
