from flask import Blueprint, current_app, jsonify, request


# Blueprint Definition
blueprint_tbd = Blueprint('blueprint_tbd', import_name=__name__)


@blueprint_tbd.route('/test', methods=['GET'])
def test():
    """
    This endpoint is being excluded from API spec
    get:
      description: tbd test endpoint
      responses:
        '200':
          description: request successful
          content:
            application/json:
              schema: TestOutputSchema
      tags:
      - test
    """
    current_app.logger.debug('test endpoint of blueprint_tbd')
    output = {'message': 'test endpoint of blueprint_tbd'}
    return jsonify(output)
