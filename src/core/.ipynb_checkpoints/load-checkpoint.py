import pandas as pd
import numpy as np
import logging


def load_data(full_path, column_header=None, drop_list=None):
    """
        Load dataset, drop column(s) and split dataset

        Arguments:
        full_path -- dataset file path
        column_header -- list of column header
        drop_list -- list of attributes to drop

        Return:
        df -- Pandas dataframe of loaded dataset
    """
    if not isinstance(column_header, list):
        if column_header is not None:
            logging.warning('column_header must be a list or None')
        column_header = []

    if not isinstance(drop_list, list):
        if drop_list is not None:
            logging.warning('drop_list must be a list or None')
        drop_list = []

    df = pd.read_fwf(full_path, header=None)

    if df.shape[1] == len(column_header):
        df.columns = column_header
    else:
        logging.warning('Column header will not be assigned to the dataframe. Reason: the number of columns does not match column header.')

    df = df.drop(np.array(drop_list), axis=1)

    return df


"""
    if drop_list is None: 
        drop_list = []
    if target_column.strip():
        drop_list.append(target_column)

    df = pd.read_feather(full_path)
    X = df.drop(np.array(drop_list), axis=1)
    y = df[[target_column]]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, 
                                                        random_state=random_state)
    return X_train, X_test, y_train, y_test
"""

def split_data(df, drop_list=None, target_column='Class', test_size=0.1, random_state=42):
    """
        Split dataset

        Arguments:
        df: Pandas dataframe
        drop_list: list of attributes to drop
        target_column: target column name
        test_size: propotion of dataset to include in the test split range [0, 1]
        random_state: random seed

        Return:
        X_train, X_test, y_train, y_test
    """
    pass
