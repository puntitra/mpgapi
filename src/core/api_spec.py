"""
Filename: api_spec.py
Description: Defines API specification (OpenAPI v3)
"""

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields


# APISpec
spec = APISpec(title='MPG API Specification',
               version='0.1.1',
               openapi_version='3.0.3',
               plugins=[FlaskPlugin(), MarshmallowPlugin()],
              )


# Input schemas for mpg/predict
class PredictInputSchema(Schema):
    cylinders = fields.Int(required=True)
    displacement = fields.Float(required=True)
    horsepower = fields.Float(required=True)
    weight = fields.Float(required=True)
    acceleration = fields.Float(required=True)
    year = fields.Int(required=True,
                      validate=lambda x: x>=0 and x <=99)
    origin = fields.Int(required=True,
                        validate=lambda x: x>=0 and x<=3)
    name = fields.Str(required=True)


# Output schema for mpg/predict
class PredictOutputSchema(Schema):
    prediction = fields.Float(required=True)


# Output schema for test (both tbd/test and mpg/test)
class TestOutputSchema(Schema):
    message = fields.Str(required=True)


# Register schemas with APISpec
# spec.components.schema(name='PredictInputSchema', schema=PredictInputSchema)
spec.components.schema(name='PredictOutputSchema', schema=PredictOutputSchema)
spec.components.schema(name='TestOutputSchema', schema=TestOutputSchema)


# Swagger tags for endpoint annotation in swagger UI
# Notes: 'name' must match 'tags' in yml-docstrings
#        API doc will appear in this exact order.
tags = [{'name': 'predict',
         'description': 'Gas mileage prediction'},
        {'name': 'test',
         'description': 'Simple endpoint health test'},
       ]


# Add tags to APISpec
for tag in tags:
    spec.tag(tag)
