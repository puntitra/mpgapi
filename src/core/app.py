"""
Filename: app.py
Description: Flask creation and registering blueprints
"""

from os import path, environ
from flask import Flask
# from flasgger import Swagger
from config import DevConfig, ProdConfig
from src.core.log import config_logger


def create_app(config=ProdConfig):
    """
    Create Flask application.

    Argument:
    config -- config object (optional)

    Return:
    app -- Flask application
    """

    # Initialize application.
    app = Flask(__name__)

    # Load configuration from config file
    app.config.from_object(config)

    # Setup logger
    log_level = app.config['LOG_LEVEL']
    log_filename = app.config['LOG_FILENAME']
    logger = config_logger(level=log_level,
                           log_filename=log_filename)

    #### Global libraries (if any) ####
    ###################################

    with app.app_context():

        # Import error handlers. Note: it uses the current application object.
        import core.errors

        ###################### Initialize Plugins ######################
        ################################################################

        ###################### Import Blueprints #######################
        logger.info('Importing Blueprints')
        from blueprints.blueprint_mpg import blueprint_mpg
        from blueprints.blueprint_tbd import blueprint_tbd
        from blueprints.blueprint_swaggerui import blueprint_swaggerui
        from blueprints.blueprint_swaggerui import SWAGGER_URL
        ################################################################

        ###################### Register Blueprints #####################
        logger.info('Registering Blueprint: {}'.format('blueprint_mpg'))
        app.register_blueprint(blueprint_mpg, url_prefix='/mpg')

        logger.info('Registering Blueprint: {}'.format('blueprint_tbd'))
        app.register_blueprint(blueprint_tbd, url_prefix='/tbd')

        logger.info('Registering Blueprint: {}'.format('blueprint_swaggerui'))
        app.register_blueprint(blueprint_swaggerui, url_prefix=SWAGGER_URL)
        ################################################################

        return app
