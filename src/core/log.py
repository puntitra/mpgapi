"""
Filename: log.py
Description: Logger configuration
"""

from logging.config import dictConfig
from logging import getLogger


def config_logger(name='', level='ERROR', log_filename='log.txt'):

    level = level.upper()

    dictConfig({
        'version': 1,
        'loggers': {
            '': {  # root logger
                'level': level,
                'handlers': ['rotating_file_handler'],
            },
            'my.package': {  # For future
                'level': level,
                'propagate': False,
                'handlers': ['rotating_file_handler'],
            },
        },
        'handlers': {
            'rotating_file_handler': {
                'level': level,
                'formatter': 'short_format',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': log_filename,
                'mode': 'a',
                'maxBytes': 1048576,
                'backupCount': 1
            }
        },
        'formatters': {
            'short_format': {
                'format': '[%(asctime)s] %(levelname)s :: %(module)s.%(funcName)s :: %(message)s',
                'datefmt': '%m/%d %H:%M:%S'
            },
            'long_format': {
                'format': '%(asctime)s-%(levelname)s-%(name)s-%(process)d::%(module)s|%(funcName)s|%(lineno)s:: %(message)s',
                'datefmt': '%m/%d %H:%M:%S'
            },
        },
    })
    return getLogger(name)
