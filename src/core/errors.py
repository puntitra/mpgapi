"""
Filename: errors.py
Description: Error handling functions
"""
from flask import current_app, json, jsonify
from werkzeug.exceptions import HTTPException, InternalServerError


# TODO: Custom Exception class
# Specifically handle non-HTTPExceptions


@current_app.errorhandler(Exception)
def handle_all_exception(e):
    """
    Returns JSON for Non-HTTP errors.

    Note: Log error details but return generic InternalServerError to clients in production.
    TODO: Customize message for common errors (with slightly more detail for clients)
    """
    # Pass through HTTP errors
    if isinstance(e, HTTPException):
        return e
    else:
        e_text = '(Internal) {}: {}'.format(type(e).__name__, str(e))
        # More detail on error for log
        current_app.logger.error('Handling Non-HTTPException -- ' + e_text)
        e = InternalServerError(original_exception=e)

    if current_app.debug:
        # Return error details if in debug mode (development)
        return jsonify(error=500, text=e_text), 500
    else:
        # Return generic InternalServerError to clients (production)
        return jsonify(error=500, text=str(e)), 500


@current_app.errorhandler(HTTPException)
def handle_http_exception(e):
    """
    Returns JSON for HTTP errors.
    """
    current_app.logger.error('Handling HTTPException -- Code ' + str(e.code))
    # Header
    response = e.get_response()
    # Modify body
    response.data = json.dumps({'code': e.code,
                                'name': e.name,
                                'description': e.description,
                              })
    response.content_type = 'application/json'

    return response
